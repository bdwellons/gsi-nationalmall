import { Component, OnInit } from '@angular/core';

// Utils
import * as _ from 'lodash';
declare var $: any;

@Component({
	  selector: 'app-menu',
	  templateUrl: './menu.component.html',
	  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

	menuData: any;
	latitude:number = 38.889685;
	longitude:number = -77.035963;
	zoom:number = 14;
	locations: any;
	iconURL: string = 'http://brantwellons.com/map/marker5.png';
	currentMenu: any = {};

	constructor() { }

	ngOnInit() {
		this.setMenuData();
	}

	markerClick(event: any, index: number) {
		this.currentMenu = _.find(this.menuData, (item) => {
			return item.id === index;
		}).pdf;

		window.open(this.currentMenu);
		console.log('current Menu: ', this.currentMenu);
		// $('.modal').modal('show');
	}

	getImagePath(image) {
		return `../../../assets${image}`;
	}

	setMenuData() {
		console.log('Setting Data')
		this.locations = [{
			id: 1,
			coords: { latitude: 38.890369, longitude: -77.030029 },
			options: { title: "American History" }
		}, {
			id: 2,
			coords: { latitude: 38.890411, longitude: -77.026006 },
			options: { title: "Nat History Museum" }
		}, {
			id: 3,
			coords: { latitude: 38.888858, longitude: -77.019851 },
			options: { title: "Air & Space Museum" }
		}, {
			id: 4,
			coords: { latitude: 38.888896, longitude: -77.024525 },
			options: { title: "Arts & Industry" }
		}, {
			id: 5,
			coords: { latitude: 38.887284, longitude: -77.048303 },
			options: { title: "Lincoln South" }
		}, {
			id: 6,
			coords: { latitude: 38.890255, longitude: -77.049448 },
			options: { title: "Lincoln North" }
		}, {
			id: 7,
			coords: { latitude: 38.891081, longitude: -77.04445 },
			options: { title: "Constitution Gardens" }
		}, {
			id: 8,
			coords: { latitude: 38.88492, longitude: -77.034665 },
			options: { title: "Tidal Basin" }
		}, {
			id: 9,
			coords: { latitude: 38.879941, longitude: -77.037019 },
			options: { title: "Jefferson" }
		}];

		this.menuData = [{
			id: 1,
			pdf: "assets/menus/AmericanHistoryKioskMenu.pdf",
			name: "American History Museum Food Kiosk",
			description: "Daily: 9:30am - 6pm",
			blurb: "Your choice of one main item and one side will be less than 600 calories",
			fitPicks: [{
				name: "Mains",
				items: [{
					name: "Southwest Roast Turkey Pita",
					price: "6.95"
				}, {
					name: "Veggie Wrap",
					price: "6.50"
				}, {
					name: "Chicken Sausage w/ Peppers & Onions (on whole wheat bun)",
					price: "4.95"
				}, {
					name: "Wasabi Chicken Wrap",
					price: "6.95"
				}]
			}, {
				name: "Sides",
				items: [{
					name: "Fat-Free Strawberry Yogurt",
					price: "1.95"
				}, {
					name: "Fresh Apple Slices",
					price: "1.35"
				}, {
					name: "All Natural Pop Chips",
					price: "1.25"
				}, {
					name: "Fresh Fruit Cup",
					price: "4.00"
				}, {
					name: "Fresh Carrot Sticks With Fat-Free Ranch Dressing",
					price: "1.35"
				}]
			}],
			limitedTime: {
				title: "Limited Time Offer",
				subTitle: "Sausage Mushroom Pizza",
				price: "$9.00",
				image: "/extras/SausageMushroomPizza-sm.jpg"
			},
			menuItems: [{
				name: "Breakfast",
				items: [{
					name: "Egg, Sausage & Cheese Croissant",
					price: "5.00"
				}, {
					name: "Gluten-Free Blueberry Muffin",
					price: "3.00"
				}, {
					name: "Instant Oatmeal Cup",
					price: "3.00"
				}]
			}, {
				name: "Mains",
				items: [{
					name: "Nathan's All Beef Hot Dogs",
					price: "4.20"
				}, {
					name: "Veggie Pita",
					price: "5.50"
				}, {
					name: "Ham & Swiss Sub",
					price: "6.25"
				}, {
					name: "Turkey & Swiss Sub",
					price: "6.00"
				}, {
					name: "Nachos w/ Chili & Cheese",
					price: "4.95"
				}, {
					name: "Chicken Caesar Salad",
					price: "7.75"
				}, {
					name: "Cheese Pizza",
					price: "6.50"
				}, {
					name: "Pepperoni Pizza",
					price: "8.00"
				}, {
					name: "Vegetable Pizza (Green Peppers, Onions, Mushrooms)",
					price: "8.00"
				}]
			}, {
				name: "Snacks",
				items: [{
					name: "Chips",
					price: "1.25"
				}, {
					name: "Hot Jumbo Pretzel",
					price: "3.00"
				}, {
					name: "White Cheddar Popcorn",
					price: "1.35"
				}, {
					name: "Giant Chocolate Chip Cookie",
					price: "1.95"
				}, {
					name: "Bagel (Plain)",
					price: "1.35"
				}, {
					name: "Bagel (Cream Cheese)",
					price: "1.90"
				}, {
					name: "Muffin (Blueberry or Chocolate Chip)",
					price: "1.95"
				}, {
					name: "Assorted Candy",
					price: "1.65"
				}, {
					name: "Pecan & Apple Mini Pies",
					price: "3.00"
				}, {
					name: "Kashi Granola Flax Seed Bar",
					price: "3.00"
				}, {
					name: "Cozy Shack Chocolate Pudding",
					price: "2.00"
				}, {
					name: "Rice Crispy Treat",
					price: "2.00"
				}, {
					name: "Eli's Cheesecake",
					price: "3.75"
				}, {
					name: "Roasted Red Pepper Hummus",
					price: "4.00"
				}]
			}, {
				name: "Ice Cream",
				items: [{
					name: "DOVEBAR (Vanilla w/ Milk Chocolate)",
					price: "3.25"
				}, {
					name: "Robo Pop Popsicle",
					price: "2.00"
				}, {
					name: "Italian Ice Cup (Lemon or Cherry)",
					price: "3.25"
				}, {
					name: "Ice Cream Sandwich",
					price: "1.75"
				}]
			}, {
				name: "Beverages",
				items: [{
					name: "Naked Juice",
					price: "4.00"
				}, {
					name: "Bottled Water",
					price: "2.50"
				}, {
					name: "Juice (Orange or Apple)",
					price: "2.45"
				}, {
					name: "Fresh Lemonade",
					price: "3.50"
				}, {
					name: "Milk",
					price: "1.75"
				}, {
					name: "Hot Tea/Coffee",
					price: "1.85"
				}, {
					name: "Hot Chocolate",
					price: "2.00"
				}, {
					name: "Soda/Iced Tea 12oz",
					price: "1.65"
				}, {
					name: "Soda/Iced Tea 20oz",
					price: "2.40"
				}, {
					name: "Beer",
					price: "3.95"
				}, {
					name: "Wine",
					price: "5.00"
				}, {
					name: "Souvenir Cup",
					price: "2.45"
				}, {
					name: "Souvenir Cup w/ Soda/Iced Tea",
					price: "4.50"
				}, {
					name: "Souvenir Cup w/ Fresh Lemonade",
					price: "5.35"
				}]
			}],
			combos: [{
				name: "#1 Kids Meal",
				desc: "Nathan's All Beef Hot Dog, Chips & 12oz Soda",
				price: "6.80",
				image: "/amHistory/1.jpg"
			}, {
				name: "#2 Deli Combo",
				desc: "Ham or Turkey Sub, Chips & 20oz Soda",
				price: "9.60",
				image: "/amHistory/2.jpg"
			}, {
				name: "#3 Souvenir Combo",
				desc: "Nathan's All Beef Hot Dog, Fries & Souvenir Cup Soda",
				price: "11.50",
				image: "/amHistory/3.jpg"
			}, {
				name: "#4 Hot Dog Combo",
				desc: "Nathan's All Beef Hot Dog, Fries & 20oz Soda",
				price: "9.40",
				image: "/amHistory/4.jpg"
			}, {
				name: "#5 Chicken Sausage Combo",
				desc: "Chicken Sausage, Fries & 20oz Soda",
				price: "10.15",
				image: "/amHistory/5.jpg"
			}, {
				name: "#6 Chicken Sausage Combo",
				desc: "Chicken Sausage, Chips & 20oz Soda",
				price: "8.25",
				image: "/amHistory/6.jpg"
			}]
		}, {
			id: 2,
			pdf: "assets/menus/NaturalHistoryKioskMenu.pdf",
			name: "Natural History Museum Food Kiosk",
			description: "Daily: 9:30am - 6pm",
			blurb: "Your choice of one main item and one side will be less than 600 calories",
			fitPicks: [{
				name: "Mains",
				items: [{
					name: "Southwest Roast Turkey Pita",
					price: "6.95"
				}, {
					name: "Veggie Wrap",
					price: "6.50"
				}, {
					name: "Chicken Sausage w/ Peppers & Onions (on whole wheat bun)",
					price: "4.95"
				}, {
					name: "Wasabi Chicken Wrap",
					price: "6.95"
				}]
			}, {
				name: "Sides",
				items: [{
					name: "Fat-Free Strawberry Yogurt",
					price: "1.95"
				}, {
					name: "Fresh Apple Slices",
					price: "1.35"
				}, {
					name: "All Natural Pop Chips",
					price: "1.25"
				}, {
					name: "Fresh Carrot Sticks With Fat-Free Ranch Dressing",
					price: "1.35"
				}]
			}],
			limitedTime: {
				title: "Limited Time Offer",
				subTitle: "Chicken Chili with White Beans and Cornbread",
				price: "$8.00",
				image: "/extras/ChickenChiliWhiteBeanSoup-CornBread.jpg"
			},
			menuItems: [{
				name: "Breakfast",
				items: [{
					name: "Egg, Sausage & Cheese Croissant",
					price: "5.00"
				}, {
					name: "Gluten-Free Blueberry Muffin",
					price: "3.00"
				}, {
					name: "Instant Oatmeal Cup",
					price: "3.00"
				}]
			}, {
				name: "Mains",
				items: [{
					name: "Nathan's All Beef Hot Dog",
					price: "4.20"
				}, {
					name: "BBQ Sandwich (Pork or Chicken)",
					price: "6.00"
				}, {
					name: "Ham & Swiss Sub",
					price: "6.25"
				}, {
					name: "Turkey & Swiss Sub",
					price: "6.00"
				}, {
					name: "Veggie Pita",
					price: "5.50"
				}, {
					name: "Chicken Caesar Salad",
					price: "7.75"
				}, {
					name: "Nachos w/ Chili & Cheese",
					price: "4.95"
				}]
			}, {
				name: "Snacks",
				items: [{
					name: "Chips",
					price: "1.25"
				}, {
					name: "Hot Jumbo Pretzel",
					price: "3.00"
				}, {
					name: "Fresh Popcorn",
					price: "3.50"
				}, {
					name: "Giant Chocolate Chip Cookie",
					price: "1.95"
				}, {
					name: "Bagel (Plain)",
					price: "1.35"
				}, {
					name: "Bagel (Cream Cheese)",
					price: "1.90"
				}, {
					name: "Muffin (Blueberry or Chocolate Chip)",
					price: "1.95"
				}, {
					name: "Assorted Candy",
					price: "1.65"
				}, {
					name: "Cherry & Apple Mini Pies",
					price: "3.00"
				}, {
					name: "Kashi Granola Flax Seed Bar",
					price: "3.00"
				}, {
					name: "Cozy Shack Chocolate Pudding",
					price: "2.00"
				}, {
					name: "Rice Crispy Treat",
					price: "2.00"
				}, {
					name: "Eli's Cheesecake",
					price: "3.75"
				}, {
					name: "Classic Hummus",
					price: "4.00"
				}]
			}, {
				name: "Ice Cream",
				items: [{
					name: "DOVEBAR (Vanilla w/ Milk Chocolate)",
					price: "3.25"
				}, {
					name: "Robo Pop Popsicle",
					price: "2.00"
				}, {
					name: "Italian Ice Cup (Lemon or Cherry)",
					price: "3.25"
				}, {
					name: "Ice Cream Sandwich",
					price: "1.75"
				}]
			}, {
				name: "Beverages",
				items: [{
					name: "Naked Juice",
					price: "4.00"
				}, {
					name: "Bottled Water",
					price: "2.50"
				}, {
					name: "Juice (Orange or Apple)",
					price: "2.45"
				}, {
					name: "Fresh Lemonade",
					price: "3.50"
				}, {
					name: "Milk",
					price: "1.75"
				}, {
					name: "Hot Tea/Coffee",
					price: "1.85"
				}, {
					name: "Hot Chocolate",
					price: "2.00"
				}, {
					name: "Soda/Iced Tea 12oz",
					price: "1.65"
				}, {
					name: "Soda/Iced Tea 20oz",
					price: "2.40"
				}, {
					name: "Beer",
					price: "3.95"
				}, {
					name: "Wine",
					price: "5.00"
				}, {
					name: "Souvenir Cup",
					price: "2.45"
				}, {
					name: "Souvenir Cup w/ Soda/Iced Tea",
					price: "4.50"
				}, {
					name: "Souvenir Cup w/ Fresh Lemonade",
					price: "5.35"
				}]
			}],
			combos: [{
				name: "#1 Kids Meal",
				desc: "Nathan's All Beef Hot Dog, Chips & 12oz Soda",
				price: "6.80",
				image: "/natHistory/1.jpg"
			}, {
				name: "#2 Deli Combo",
				desc: "Ham or Turkey, Chips & 20oz Soda",
				price: "9.60",
				image: "/natHistory/2.jpg"
			}, {
				name: "#3 Souvenir Combo",
				desc: "Nathan's All Beef Hot Dog, Fries, & Souvenir Cup Soda",
				price: "11.50",
				image: "/natHistory/3.jpg"
			}, {
				name: "#4 Hot Dog Combo",
				desc: "Nathan's All Beef Hot Dog, Fries & 20oz Soda",
				price: "9.40",
				image: "/natHistory/4.jpg"
			}, {
				name: "#5 BBQ Combo w/ Fries",
				desc: "BBQ Chicken or Pork Sandwich, Fries & 20oz Soda",
				price: "11.25",
				image: "/natHistory/5.jpg"
			}, {
				name: "#6 BBQ Combo w/ Chips",
				desc: "BBQ Chicken or Pork Sandwich, Chips & 20oz Soda",
				price: "9.30",
				image: "/natHistory/6.jpg"
			}]
		}, {
			id: 3,
			pdf: "assets/menus/AirAndSpaceMuseumKioskMenu.pdf",
			name: "Air & Space Museum Food Kiosk",
			description: "Daily: 9:30am - 6pm",
			blurb: "Your choice of one main item and one side will be less than 600 calories",
			fitPicks: [{
				name: "Mains",
				items: [{
					name: "Southwest Roast Turkey Pita",
					price: "6.95"
				}, {
					name: "Veggie Wrap",
					price: "6.50"
				}, {
					name: "Chicken Sausage w/ Peppers & Onions (on whole wheat bun)",
					price: "4.95"
				}, {
					name: "Wasabi Chicken Wrap",
					price: "6.95"
				}, {
					name: "Grilled Chicken Sandwich (on whole wheat bun)",
					price: "9.00"
				}]
			}, {
				name: "Sides",
				items: [{
					name: "Fat-Free Strawberry Yogurt",
					price: "1.95"
				}, {
					name: "Fresh Apple Slices",
					price: "1.35"
				}, {
					name: "All Natural Pop Chips",
					price: "1.25"
				}, {
					name: "Fresh Fruit Cup",
					price: "4.00"
				}, {
					name: "Fresh Carrot Sticks With Fat-Free Ranch Dressing",
					price: "1.35"
				}]
			}],
			menuItems: [{
				name: "Breakfast",
				items: [{
					name: "Egg, Sausage & Cheese Croissant",
					price: "5.00"
				}, {
					name: "Gluten-Free Blueberry Muffin",
					price: "3.00"
				}, {
					name: "Instant Oatmeal Cup",
					price: "3.00"
				}]
			}, {
				name: "Mains",
				items: [{
					name: "Nathan's All Beef Hot Dog",
					price: "4.20"
				}, {
					name: "Veggie Pita",
					price: "5.50"
				}, {
					name: "Ham & Swiss Sub",
					price: "6.25"
				}, {
					name: "Turkey & Swiss Sub",
					price: "6.00"
				}, {
					name: "Nachos w/ Chili & Cheese",
					price: "4.95"
				}, {
					name: "Chicken Caesar Salad",
					price: "7.75"
				}, {
					name: "6-pc. Chicken Wings",
					price: "7.00"
				}, {
					name: "12-pc Chicken Wings",
					price: "12.00"
				}]
			}, {
				name: "Snacks",
				items: [{
					name: "Chips",
					price: "1.25"
				}, {
					name: "Hot Jumbo Pretzel",
					price: "3.00"
				}, {
					name: "White Cheddar Popcorn",
					price: "1.35"
				}, {
					name: "Giant Chocolate Chip Cookie",
					price: "1.95"
				}, {
					name: "Bagel (Plain)",
					price: "1.35"
				}, {
					name: "Bagel (Cream Cheese)",
					price: "1.90"
				}, {
					name: "Muffin (Blueberry or Chocolate Chip)",
					price: "1.95"
				}, {
					name: "Assorted Candy",
					price: "1.65"
				}, {
					name: "Pecan & Cherry Mini Pies",
					price: "3.00"
				}, {
					name: "Kashi Granola Flax Seed Bar",
					price: "3.00"
				}, {
					name: "Cozy Shack Chocolate Pudding",
					price: "2.00"
				}, {
					name: "Rice Crispy Treat",
					price: "2.00"
				}, {
					name: "Eli's Cheesecake",
					price: "3.75"
				}, {
					name: "Roasted Red Pepper Hummus",
					price: "4.00"
				}]
			}, {
				name: "Ice Cream",
				items: [{
					name: "DOVEBAR (Vanilla w/ Milk Chocolate)",
					price: "3.25"
				}, {
					name: "Robo Pop Popsicle",
					price: "2.00"
				}, {
					name: "Italian Ice Cup (Lemon or Cherry)",
					price: "3.25"
				}, {
					name: "Ice Cream Sandwich",
					price: "1.75"
				}]
			}, {
				name: "Beverages",
				items: [{
					name: "Naked Juice",
					price: "4.00"
				}, {
					name: "Bottled Water",
					price: "2.50"
				}, {
					name: "Juice (Orange or Apple)",
					price: "2.45"
				}, {
					name: "Fresh Lemonade",
					price: "3.50"
				}, {
					name: "Milk",
					price: "1.75"
				}, {
					name: "Hot Tea/Coffee",
					price: "1.85"
				}, {
					name: "Hot Chocolate",
					price: "2.00"
				}, {
					name: "Soda/Iced Tea 12oz",
					price: "1.65"
				}, {
					name: "Soda/Iced Tea 20oz",
					price: "2.40"
				}, {
					name: "Beer",
					price: "3.95"
				}, {
					name: "Wine",
					price: "5.00"
				}, {
					name: "Souvenir Cup",
					price: "2.45"
				}, {
					name: "Souvenir Cup w/ Soda/Iced Tea",
					price: "4.50"
				}, {
					name: "Souvenir Cup w/ Fresh Lemonade",
					price: "5.35"
				}]
			}],
			combos: [{
				name: "#1 Kids Meal",
				desc: "Nathan's All Beef Hot Dog, Chips & 12oz Soda",
				price: "6.80",
				image: "/airSpace/1.jpg"
			}, {
				name: "#2 Deli Combo",
				desc: "Ham or Turkey Sub, Chips & 20oz Soda",
				price: "9.60",
				image: "/airSpace/2.jpg"
			}, {
				name: "#3 Souvenir Combo",
				desc: "Nathan's All Beef Hot Dog, Fries & Souvenir Cup Soda",
				price: "11.50",
				image: "/airSpace/3.jpg"
			}, {
				name: "#4 Hot Dog Combo",
				desc: "Nathan's All Beef Hot Dog, Fries & 20oz Soda",
				price: "9.40",
				image: "/airSpace/4.jpg"
			}, {
				name: "#5 Chicken Sausage Combo",
				desc: "Chicken Sausage, Fries & 20oz Soda",
				price: "10.15",
				image: "/airSpace/5.jpg"
			}, {
				name: "#6 Chicken Sausage Combo",
				desc: "Chicken Sausage, Chips & 20oz Soda",
				price: "8.25",
				image: "/airSpace/6.jpg"
			}, {
				name: "#7 Chicken Wings Combo",
				desc: "6-Pc Chicken Wings, Fries & 20oz Soda",
				price: "12.00",
				image: "/airSpace/7.jpg"
			}]
		}, {
			id: 4,
			pdf: "assets/menus/ArtsAndIndustryKioskMenu.pdf",
			name: "Arts and Industries Museum Food Kiosk",
			description: "Daily: 9:30am - 6pm",
			blurb: "Your choice of one main item and one side will be less than 600 calories",
			fitPicks: [{
				name: "Mains",
				items: [{
					name: "Southwest Roast Turkey Pita",
					price: "6.95"
				}, {
					name: "Chicken Sausage w/ Peppers & Onions (on whole wheat bun)",
					price: "4.95"
				}, {
					name: "Wasabi Chicken Wrap",
					price: "6.95"
				}, {
					name: "Veggie Wrap",
					price: "6.50"
				}]
			}, {
				name: "Sides",
				items: [{
					name: "Fat-Free Strawberry Yogurt",
					price: "1.95"
				}, {
					name: "Fresh Apple Slices",
					price: "1.35"
				}, {
					name: "All Natural Pop Chips",
					price: "1.25"
				}, {
					name: "Fresh Fruit Cup",
					price: "4.00"
				}, {
					name: "Fresh Carrot Sticks",
					price: "1.35"
				}]
			}],
			limitedTime: {
				title: "Limited Time Offer",
				subTitle: "Crab Cakes",
				price: "$16.00",
				image: "/extras/CrabCakeSandwich.jpg"
			},
			menuItems: [{
				name: "Breakfast",
				items: [{
					name: "Egg, Sausage & Cheese Croissant",
					price: "5.00"
				}, {
					name: "Gluten-Free Blueberry Muffin",
					price: "3.00"
				}, {
					name: "Instant Oatmeal Cup",
					price: "3.00"
				}]
			}, {
				name: "Mains",
				items: [{
					name: "Nathan's All Beef Hot Dog",
					price: "4.20"
				}, {
					name: "Footlong Coney Island Hot Dog",
					price: "4.95"
				}, {
					name: "Shrimp & Chips",
					price: "11.00"
				}, {
					name: "Ham & Swiss Sub",
					price: "6.25"
				}, {
					name: "Turkey & Swiss Sub",
					price: "6.00"
				}, {
					name: "Nachos w/ Chili & Cheese",
					price: "4.95"
				}, {
					name: "Chicken Caesar Salad",
					price: "7.75"
				}, {
					name: "Veggie Pita",
					price: "5.50"
				}, {
					name: "Alaska Pollock & Chips Boat",
					price: "9.00"
				}, {
					name: "Hot Dog Toppings: Cheese, Chili, Onions",
					price: ".75/ea"
				}]
			}, {
				name: "Snacks",
				items: [{
					name: "Chips",
					price: "1.25"
				}, {
					name: "Hot Jumbo Pretzel",
					price: "3.00"
				}, {
					name: "White Cheddar Popcorn",
					price: "1.35"
				}, {
					name: "Giant Chocolate Chip Cookie",
					price: "1.95"
				}, {
					name: "Bagel (Plain)",
					price: "1.35"
				}, {
					name: "Bagel (Cream Cheese)",
					price: "1.90"
				}, {
					name: "Muffin (Blueberry or Chocolate Chip)",
					price: "1.95"
				}, {
					name: "Assorted Candy",
					price: "1.65"
				}, {
					name: "Funnel Cake",
					price: "6.00"
				}, {
					name: "SunChips",
					price: "1.25"
				}, {
					name: "Pecan & Apple Mini Pies",
					price: "3.00"
				}, {
					name: "Kashi Granola Flax Seed Bar",
					price: "3.00"
				}, {
					name: "Cozy Shack Chocolate Pudding",
					price: "2.00"
				}, {
					name: "Rice Crispy Treat",
					price: "2.00"
				}, {
					name: "Eli's Cheesecake",
					price: "3.75"
				}, {
					name: "Classic Hummus",
					price: "4.00"
				}]
			}, {
				name: "Ice Cream",
				items: [{
					name: "DOVEBAR (Vanilla w/ Milk Chocolate)",
					price: "3.25"
				}, {
					name: "Robo Pop Popsicle",
					price: "2.00"
				}, {
					name: "Italian Ice Cup (Lemon or Cherry)",
					price: "3.25"
				}, {
					name: "Ice Cream Sandwich",
					price: "1.75"
				}]
			}, {
				name: "Beverages",
				items: [{
					name: "Naked Juice",
					price: "4.00"
				}, {
					name: "Bottled Water",
					price: "2.50"
				}, {
					name: "Juice (Orange or Apple)",
					price: "2.45"
				}, {
					name: "Fresh Lemonade",
					price: "3.50"
				}, {
					name: "Milk",
					price: "1.75"
				}, {
					name: "Hot Tea/Coffee",
					price: "1.85"
				}, {
					name: "Hot Chocolate",
					price: "2.00"
				}, {
					name: "Soda/Iced Tea 12oz",
					price: "1.65"
				}, {
					name: "Soda/Iced Tea 20oz",
					price: "2.40"
				}, {
					name: "Beer",
					price: "3.95"
				}, {
					name: "Wine",
					price: "5.00"
				}, {
					name: "Souvenir Cup",
					price: "2.45"
				}, {
					name: "Souvenir Cup w/ Soda/Iced Tea",
					price: "4.50"
				}, {
					name: "Souvenir Cup w/ Fresh Lemonade",
					price: "5.35"
				}]
			}],
			combos: [{
				name: "#1 Kids Meal",
				desc: "Nathan's All Beef Hot Dog, Chips & 12oz Soda",
				price: "6.80",
				image: "/artsIndustry/1.jpg"
			}, {
				name: "#2 Deli Combo",
				desc: "Ham or Turkey Sub, Chips & 20oz Soda",
				price: "9.60",
				image: "/artsIndustry/2.jpg"
			}, {
				name: "#3 Footlong Combo",
				desc: "Footlong Coney Island Hot Dog, Fries & 20oz Soda",
				price: "10.15",
				image: "/artsIndustry/3.jpg"
			}, {
				name: "#4 Chicken Sausage Combo",
				desc: "Chicken Sausage, Fries & 20oz Soda",
				price: "10.15",
				image: "/artsIndustry/4.jpg"
			}, {
				name: "#5 Chicken Sausage Combo",
				desc: "Chicken Sausage, Chips & 20oz Soda",
				price: "8.25",
				image: "/artsIndustry/5.jpg"
			}, {
				name: "#6 Hot Dog Souvenir Combo",
				desc: "Nathan's All Beef Hot Dog, Fries & Souvenir Cup Soda",
				price: "11.50",
				image: "/artsIndustry/6.jpg"
			}, {
				name: "#7 Fish & Chips Combo",
				desc: "Alaska Pollock & Chips & 20oz Soda",
				price: "11.10",
				image: "/artsIndustry/7.jpg"
			}, {
				name: "#8 Shrimp & Chips Combo",
				desc: "Shrimp & Chips & 20oz Soda",
				price: "13.00",
				image: "/artsIndustry/8.jpg"
			}]
		}, {
			id: 5,
			pdf: "assets/menus/LincolnKioskMenu.pdf",
			name: "Lincoln Memorial South Food Kiosk",
			description: "Daily: 8am - 9pm",
			blurb: "Your choice of one main item and one side will be less than 600 calories",
			fitPicks: [{
				name: "Mains",
				items: [{
					name: "Southwest Turkey Pita",
					price: "6.95"
				}, {
					name: "Veggie Wrap",
					price: "6.50"
				}, {
					name: "Wasabi Chicken Wrap",
					price: "6.95"
				}]
			}, {
				name: "Sides",
				items: [{
					name: "Fat-Free Strawberry Yogurt",
					price: "1.95"
				}, {
					name: "Fresh Apple Slices",
					price: "1.35"
				}, {
					name: "All Natural Pop Chips",
					price: "1.25"
				}, {
					name: "Fresh Fruit Cup",
					price: "4.00"
				}, {
					name: "Fresh Carrot Sticks with Fat-Free Ranch Dressing",
					price: "1.35"
				}]
			}],
			limitedTime: {
				title: "Limited Time Offer",
				subTitle: "Bacon Cheeseburger",
				price: "$10 a la carte $14.50 combo (with Fries and Soda) ",
				image: "/extras/BaconCheeseburger.jpg"
			},
			menuItems: [{
				name: "Breakfast",
				items: [{
					name: "Egg, Sausage & Cheese Croissant",
					price: "5.00"
				}, {
					name: "Gluten-Free Blueberry Muffin",
					price: "3.00"
				}]
			}, {
				name: "Mains",
				items: [{
					name: "Nathan's All Beef Hot Dog",
					price: "4.20"
				}, {
					name: "Footlong Coney Island Hot Dog",
					price: "4.95"
				}, {
					name: "Carousel Hot Dog",
					price: "4.95"
				}, {
					name: "Spicy Chicken Sandwich",
					price: "5.25"
				}, {
					name: "Country Tuna Salad Sandwich",
					price: "6.95"
				}, {
					name: "Hamburger",
					price: "6.30"
				}, {
					name: "Cheeseburger",
					price: "7.30"
				}, {
					name: "Boca or Black Bean Burger",
					price: "7.00"
				}, {
					name: "Caesar Salad",
					price: "5.75"
				}, {
					name: "Chicken Caesar Salad",
					price: "7.95"
				}, {
					name: "Chicken Tenders w/ Fries",
					price: "7.25"
				}]
			}, {
				name: "Snacks",
				items: [{
					name: "Chips",
					price: "1.25"
				}, {
					name: "Hot Jumbo Pretzel",
					price: "3.00"
				}, {
					name: "Fresh Popcorn",
					price: "3.50"
				}, {
					name: "SunChips",
					price: "1.25"
				}, {
					name: "Giant Chocolate Chip Cookie",
					price: "1.95"
				}, {
					name: "Bagel (Plain)",
					price: "1.35"
				}, {
					name: "Bagel (Cream Cheese)",
					price: "1.90"
				}, {
					name: "Muffin (Blueberry or Chocolate Chip)",
					price: "1.95"
				}, {
					name: "Assorted Candy",
					price: "1.65"
				}, {
					name: "Pecan & Apple Mini Pies",
					price: "3.00"
				}, {
					name: "Kashi Granola Flax Seed Bar",
					price: "3.00"
				}, {
					name: "Bear Naked Energy Bar",
					price: "3.00"
				}, {
					name: "Rice Crispy Treat",
					price: "2.00"
				}, {
					name: "Eli's Cheescake",
					price: "3.75"
				}]
			}, {
				name: "Ice Cream",
				items: [{
					name: "DOVEBAR (Vanilla w/ Milk Chocolate)",
					price: "3.25"
				}, {
					name: "Robo Pop Popsicle",
					price: "2.00"
				}, {
					name: "Italian Ice Cup (Lemon or Cherry)",
					price: "3.25"
				}, {
					name: "Ice Cream Sandwich",
					price: "1.75"
				}]
			}, {
				name: "Beverages",
				items: [{
					name: "Naked Juice",
					price: "4.00"
				}, {
					name: "Bottled Water",
					price: "2.50"
				}, {
					name: "Juice (Orange or Apple)",
					price: "2.45"
				}, {
					name: "Fresh Lemonade",
					price: "3.50"
				}, {
					name: "Milk",
					price: "1.75"
				}, {
					name: "Hot Tea/Coffee",
					price: "1.85"
				}, {
					name: "Hot Chocolate",
					price: "2.00"
				}, {
					name: "Soda/Iced Tea 12oz",
					price: "1.65"
				}, {
					name: "Soda/Iced Tea 20oz",
					price: "2.40"
				}, {
					name: "Beer",
					price: "3.95"
				}, {
					name: "Wine",
					price: "5.00"
				}, {
					name: "Souvenir Cup",
					price: "2.45"
				}, {
					name: "Souvenir Cup w/ Soda/Iced Tea",
					price: "4.50"
				}, {
					name: "Souvenir Cup w/ Fresh Lemonade",
					price: "5.35"
				}]
			}],
			combos: [{
				name: "#1 Kids Meal",
				desc: "Nathan's All Beef Hot Dog, Chips & 12oz Soda",
				price: "6.80",
				image: "/lincolnSouth/1.jpg"
			}, {
				name: "#2 Deli Combo",
				desc: "Your Choice of Country Tuna or Southwest Roast Turkey Pita, Chips & 20oz Soda",
				price: "10.25",
				image: "/lincolnSouth/2.jpg"
			}, {
				name: "#3 Footlong Combo",
				desc: "Footlong Coney Island Hot Dog, Fries & 20oz Soda",
				price: "10.15",
				image: "/lincolnSouth/3.jpg"
			}, {
				name: "#4 Chicken Tenders Combo",
				desc: "Chicken Tenders, Fries & 20oz Soda",
				price: "9.40",
				image: "/lincolnSouth/4.jpg"
			}, {
				name: "#5 Spicy Chicken Sandwich Combo",
				desc: "Spicy Chicken Sandwich, Fries & 20oz Soda",
				price: "10.15",
				image: "/lincolnSouth/5.jpg"
			}, {
				name: "#6 Hot Dog Souvenir Combo",
				desc: "Nathan's All Beef Hot Dog, Fries & Souvenir Cup Soda",
				price: "11.50",
				image: "/lincolnSouth/6.jpg"
			}, {
				name: "#7 Hamburger Combo",
				desc: "Hamburger w/ Pickles, Fries & 20oz Soda",
				price: "11.50",
				image: "/lincolnSouth/7.jpg"
			}, {
				name: "#8 Cheesburger Combo",
				desc: "Cheeseburger w/ Pickles, Fries & 20oz Soda",
				price: "12.50",
				image: "/lincolnSouth/8.jpg"
			}]
		}, {
			id: 6,
			pdf: "assets/menus/LincolnKioskMenu.pdf",
			name: "Lincoln Memorial North Food Kiosk",
			description: "Daily: 8am - 10pm",
			blurb: "Your choice of main item and one side will be less than 600 calories",
			menuItems: [{
				name: "Mains",
				items: [{
					name: "Turkey & Swiss on Croissant",
					price: "7.15"
				}, {
					name: "Ham & Swiss Sub",
					price: "6.25"
				}, {
					name: "Veggie Wrap",
					price: "7.65"
				}, {
					name: "Tuna Salad - Marble Rye",
					price: "7.15"
				}, {
					name: "Wasabi Chicken Wrap",
					price: "7.15"
				}, {
					name: "Gourmet Fruit & Cheese Plate",
					price: "10.00"
				}, {
					name: "Smoked Turkey Energy Box",
					price: "9.00"
				}, {
					name: "Chicken Caesar Salad",
					price: "7.95"
				}, {
					name: "Country Cobb Salad",
					price: "7.95"
				}]
			}, {
				name: "Snacks",
				items: [{
					name: "Fruit Cup",
					price: "4.00"
				}, {
					name: "Carrot & Celery Sticks with Fat-Free Ranch Dressing",
					price: "4.00"
				}, {
					name: "Fruit & Granola Yogurt Parfait",
					price: "5.50"
				}, {
					name: "Hummus & Pretzel Cups",
					price: "4.00"
				}, {
					name: "Apple Pie",
					price: "3.00"
				}]
			}, {
				name: "Coffee Drinks",
				items: [{
					name: "Café Latte",
					price: "2.92"
				}, {
					name: "Café Mocha",
					price: "3.30"
				}, {
					name: "Cappuccino",
					price: "2.92"
				}, {
					name: "Hot Chocolate",
					price: "2.48"
				}, {
					name: "Regular Coffee",
					price: "1.85"
				}, {
					name: "Decaf Coffee",
					price: "1.85"
				}, {
					name: "Hot Tea",
					price: "1.85"
				}]
			}]
		}, {
			id: 7,
			pdf: "assets/menus/ConstitutionGardensKioskMenu.pdf",
			name: "Constitution Gardens Food Kiosk",
			description: "Daily: 9:30am - 6pm",
			blurb: "Your choice of one main item and one side will be less than 600 calories",
			fitPicks: [{
				name: "Mains",
				items: [{
					name: "Southwest Roast Turkey Pita",
					price: "6.95"
				}, {
					name: "Veggie Wrap",
					price: "6.50"
				}, {
					name: "Wasabi Chicken Wrap",
					price: "6.95"
				}]
			}, {
				name: "Sides",
				items: [{
					name: "Fat-Free Strawberry Yogurt",
					price: "1.95"
				}, {
					name: "Fresh Apple Slices",
					price: "1.35"
				}, {
					name: "All Natural Pop Chips",
					price: "1.25"
				}, {
					name: "Fresh Fruit Cup",
					price: "4.00"
				}, {
					name: "Fresh Carrot Sticks With Fat-Free Ranch Dressing",
					price: "1.35"
				}]
			}],
			menuItems: [{
				name: "Breakfast",
				items: [{
					name: "Egg, Sausage & Cheese Croissant",
					price: "5.00"
				}, {
					name: "Gluten-Free Blueberry Muffin",
					price: "3.00"
				}, {
					name: "Instant Oatmeal Cup",
					price: "3.00"
				}]
			}, {
				name: "Mains",
				items: [{
					name: "Nathan's All Beef Hot Dog",
					price: "4.20"
				}, {
					name: "Veggie Pita",
					price: "5.50"
				}, {
					name: "Turkey & Swiss Sub",
					price: "6.00"
				}, {
					name: "Ham & Swiss Sub",
					price: "6.25"
				}]
			}, {
				name: "Snacks",
				items: [{
					name: "Chips",
					price: "1.25"
				}, {
					name: "SunChips",
					price: "1.25"
				}, {
					name: "Hot Jumbo Pretzel",
					price: "3.00"
				}, {
					name: "White Cheddar Popcorn",
					price: "1.35"
				}, {
					name: "Giant Chocolate Chip Cookie",
					price: "1.95"
				}, {
					name: "Bagel",
					price: "1.35"
				}, {
					name: "Bagel (w/ Cream Cheese)",
					price: "1.90"
				}, {
					name: "Muffin (Blueberry or Chocolate Chip)",
					price: "1.95"
				}, {
					name: "Assorted Candy",
					price: "1.65"
				}, {
					name: "Pecan & Apple Mini Pies",
					price: "3.00"
				}, {
					name: "Bear Naked Energy Bar",
					price: "3.00"
				}, {
					name: "Cozy Shack Chocolate Pudding",
					price: "2.00"
				}, {
					name: "Rice Crispy Treat",
					price: "2.00"
				}, {
					name: "Eli's Cheesecake",
					price: "3.75"
				}, {
					name: "Roasted Red Pepper Hummus",
					price: "4.00"
				}]
			}, {
				name: "Ice Cream",
				items: [{
					name: "DOVEBAR (Vanilla w/ Milk Chocolate)",
					price: "3.25"
				}, {
					name: "Robo Pop Popsicle",
					price: "2.00"
				}, {
					name: "Italian Ice Cup (Lemon or Cherry)",
					price: "3.25"
				}, {
					name: "Ice Cream Sandwich",
					price: "1.75"
				}]
			}, {
				name: "Beverages",
				items: [{
					name: "Naked Juice",
					price: "4.00"
				}, {
					name: "Bottled Water",
					price: "2.50"
				}, {
					name: "Juice (Orange or Apple)",
					price: "2.45"
				}, {
					name: "Milk",
					price: "1.75"
				}, {
					name: "Hot Tea/Coffee",
					price: "1.85"
				}, {
					name: "Hot Chocolate",
					price: "2.00"
				}, {
					name: "Soda/Iced Tea 12oz",
					price: "1.65"
				}, {
					name: "Soda/Iced Tea 20oz",
					price: "2.40"
				}, {
					name: "Beer",
					price: "3.95"
				}, {
					name: "Wine",
					price: "5.00"
				}, {
					name: "Souvenir Cup w/ Soda/Iced Tea",
					price: "4.50"
				}]
			}],
			combos: [{
				name: "#1 Kids Meal",
				desc: "Nathan's All Beef Hot Dog, Chips & 12oz Soda",
				price: "6.80",
				image: "/constGarden/1.jpg"
			}, {
				name: "#2 Deli Combo",
				desc: "Ham or Turkey Sub, Chips & 20oz Soda",
				price: "9.60",
				image: "/constGarden/2.jpg"
			}, {
				name: "#3 Nathan's Meal",
				desc: "Nathan's All Beef Hot Dog, Chips & 20oz Soda",
				price: "7.55",
				image: "/constGarden/3.jpg"
			}, {
				name: "#4 Souvenir Meal",
				desc: "Nathan's All Beef Hot Dog, Chips & Souvenir Soda",
				price: "9.65",
				image: "/constGarden/4.jpg"
			}]
		}, {
			id: 8,
			pdf: "assets/menus/TidalBasinKioskMenu.pdf",
			name: "Tidal Basin Food Kiosk",
			description: "Daily: 9:30am - 6pm",
			blurb: "Your choice of one main item and one side will be less than 600 calories",
			fitPicks: [{
				name: "Mains",
				items: [{
					name: "Southwest Roast Turkey Pita",
					price: "6.95"
				}, {
					name: "Veggie Wrap",
					price: "6.50"
				}, {
					name: "Wasabi Chicken Wrap",
					price: "6.95"
				}]
			}, {
				name: "Sides",
				items: [{
					name: "Fat-Free Strawberry Yogurt",
					price: "1.95"
				}, {
					name: "Fresh Apple Slices",
					price: "1.35"
				}, {
					name: "All Natural Pop Chips",
					price: "1.25"
				}, {
					name: "Fresh Carrot Sticks With Fat-Free Ranch Dressing",
					price: "1.35"
				}]
			}],
			menuItems: [{
				name: "Mains",
				items: [{
					name: "Nathan's All Beef Hot Dog",
					price: "4.20"
				}, {
					name: "Veggie Pita",
					price: "5.50"
				}, {
					name: "Ham & Swiss Sub",
					price: "6.25"
				}]
			}, {
				name: "Snacks",
				items: [{
					name: "Chips",
					price: "1.25"
				}, {
					name: "Giant Cookie (Chocolate Chip or Oatmeal)",
					price: "1.95"
				}, {
					name: "Bagel",
					price: "1.35"
				}, {
					name: "Bagel With Cream Cheese",
					price: "1.90"
				}, {
					name: "Muffin (Blueberry, Banana Nut or Chocolate Chip)",
					price: "1.95"
				}, {
					name: "Fresh Popcorn",
					price: "3.50"
				}, {
					name: "Assorted Candy",
					price: "1.65"
				}]
			}, {
				name: "Ice Cream",
				items: [{
					name: "DOVEBAR (Vanilla w/ Milk Chocolate)",
					price: "3.25"
				}, {
					name: "Robo Pop Popsicle",
					price: "2.00"
				}, {
					name: "Italian Ice Cup (Lemon or Cherry)",
					price: "3.25"
				}, {
					name: "Ice Cream Sandwich",
					price: "1.75"
				}]
			}, {
				name: "Beverages",
				items: [{
					name: "Naked Juice",
					price: "4.00"
				}, {
					name: "Bottled Water",
					price: "2.50"
				}, {
					name: "Juice (Orange or Apple)",
					price: "2.45"
				}, {
					name: "Milk",
					price: "1.75"
				}, {
					name: "Hot Tea/Coffee",
					price: "1.85"
				}, {
					name: "Hot Chocolate",
					price: "2.00"
				}, {
					name: "Gatorade",
					price: "2.80"
				}, {
					name: "Soda/Iced Tea 12oz",
					price: "1.65"
				}, {
					name: "Soda/Iced Tea 20oz",
					price: "2.40"
				}, {
					name: "Beer",
					price: "3.95"
				}, {
					name: "Wine",
					price: "5.00"
				}, {
					name: "Souvenir Cup",
					price: "2.45"
				}]
			}],
			combos: [{
				name: "#1 Kids Meal",
				desc: "Nathan's All Beef Hot Dog, Chips & 12oz Soda",
				price: "6.80",
				image: "/tidal/1.jpg"
			}, {
				name: "#2 Deli Combo",
				desc: "Ham or Turkey Sub, Chips & 20oz Soda",
				price: "9.60",
				image: "/tidal/2.jpg"
			}, {
				name: "#3 Nathan's Meal",
				desc: "Nathan's All Beef Hot Dog, Chips & 20oz Soda",
				price: "7.55",
				image: "/tidal/3.jpg"
			}, {
				name: "#4 Souvenir Meal",
				desc: "Nathan's All Beef Hot Dog, Chips & Souvenir Cup Soda",
				price: "9.65",
				image: "/tidal/4.jpg"
			}]
		}, {
			id: 9,
			pdf: "assets/menus/JeffersonKioskMenu.pdf",
			name: "Jefferson Memorial Food Kiosk",
			description: "Daily: 9:30am - 9pm",
			blurb: "Your choice of one main item and one side will be less than 600 calories",
			fitPicks: [{
				name: "Mains",
				items: [{
					name: "Southwest Roast Turkey Pita",
					price: "6.95"
				}, {
					name: "Veggie Wrap",
					price: "6.50"
				}, {
					name: "Wasabi Chicken Wrap",
					price: "6.95"
				}]
			}, {
				name: "Sides",
				items: [{
					name: "Fat-Free Strawberry Yogurt",
					price: "1.95"
				}, {
					name: "Fresh Apple Slices",
					price: "1.35"
				}, {
					name: "All Natural Pop Chips",
					price: "1.25"
				}, {
					name: "Fresh Carrot Sticks With Fat-Free Ranch Dressing",
					price: "1.35"
				}]
			}],
			menuItems: [{
				name: "Breakfast",
				items: [{
					name: "Egg, Sausage & Cheese Croissant",
					price: "5.00"
				}, {
					name: "Gluten-Free Blueberry Muffin",
					price: "3.00"
				}, {
					name: "Instant Oatmeal Cup",
					price: "3.00"
				}]
			}, {
				name: "Mains",
				items: [{
					name: "Nathan's All Beef Hot Dog",
					price: "4.20"
				}, {
					name: "Veggie Pita",
					price: "5.50"
				}, {
					name: "Turkey & Swiss Sub",
					price: "6.00"
				}, {
					name: "Ham & Swiss Sub",
					price: "6.25"
				}]
			}, {
				name: "Snacks",
				items: [{
					name: "Chips",
					price: "1.25"
				}, {
					name: "SunChips",
					price: "1.25"
				}, {
					name: "Hot Jumbo Pretzel",
					price: "3.00"
				}, {
					name: "White Cheddar Popcorn",
					price: "1.35"
				}, {
					name: "Giant Chocolate Chip Cookie",
					price: "1.95"
				}, {
					name: "Bagel",
					price: "1.35"
				}, {
					name: "Bagel w/ Cream Cheese",
					price: "1.90"
				}, {
					name: "Muffin (Blueberry or Chocolate Chip)",
					price: "1.95"
				}, {
					name: "Assorted Candy",
					price: "1.65"
				}, {
					name: "Pecan & Apple Mini Pies",
					price: "3.00"
				}, {
					name: "Bear Naked Energy Bar",
					price: "3.00"
				}, {
					name: "Cozy Shack Chocolate Pudding",
					price: "2.00"
				}, {
					name: "Rice Crispy Treat",
					price: "2.00"
				}, {
					name: "Eli's Cheesecake",
					price: "3.75"
				}, {
					name: "Roasted Red Pepper Hummus",
					price: "4.00"
				}]
			}, {
				name: "Ice Cream",
				items: [{
					name: "DOVEBAR (Vanilla w/ Milk Chocolate)",
					price: "3.25"
				}, {
					name: "Robo Pop Popsicle",
					price: "2.00"
				}, {
					name: "Italian Ice Cup (Lemon or Cherry)",
					price: "3.25"
				}, {
					name: "Ice Cream Sandwich",
					price: "1.75"
				}]
			}, {
				name: "Beverages",
				items: [{
					name: "Bottled Water",
					price: "2.50"
				}, {
					name: "Juice (Orange or Apple)",
					price: "2.45"
				}, {
					name: "Milk",
					price: "1.75"
				}, {
					name: "Hot Tea/ Coffee",
					price: "1.85"
				}, {
					name: "Hot Chocolate",
					price: "2.00"
				}, {
					name: "Gatorade",
					price: "2.80"
				}, {
					name: "Soda/ Iced Tea (12oz)",
					price: "1.65"
				}, {
					name: "Soda/ Iced Tea (20oz)",
					price: "2.40"
				}, {
					name: "Souvenir Cup (32oz)",
					price: "4.50"
				}]
			}],
			combos: [{
				name: "#1 Kids Meal",
				desc: "Nathan's All Beef Hot Dog, Chips & 12oz Soda",
				price: "6.80",
				image: "/jefferson/1.jpg"
			}, {
				name: "#2 Deli Combo",
				desc: "Ham or Turkey Sub, Chips & 20oz Soda",
				price: "9.60",
				image: "/jefferson/2.jpg"
			}, {
				name: "#3 Nathan's Meal",
				desc: "Nathan's All Beef Hot Dog, Chips & 20oz Soda",
				price: "7.55",
				image: "/jefferson/3.jpg"
			}, {
				name: "#4 Souvenir Meal",
				desc: "Nathan's All Beef Hot Dog, Chips & Souvenir Cup Soda",
				price: "9.65",
				image: "/jefferson/4.jpg"
			}]
		}];
	}
}
